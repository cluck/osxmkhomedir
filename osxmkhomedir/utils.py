# -*- coding: utf-8 -*-

from __future__ import unicode_literals

__version__ = '4.0.0'
__author__ = 'Claudio Luck'
__author_email__ = 'claudio.luck@gmail.com'
__url__ = 'https://github.com/cluck/osxmkhomedir'

import sys
import os
import codecs
import fcntl
import getopt
import glob
import grp
import plistlib
import pwd
import select
import shutil
import subprocess
import logging
import logging.handlers

from osxmkhomedir.syncflow import State



def init_logging(logf, rollover=True):
    logger = logging.getLogger('')
    logger.setLevel(logging.DEBUG)
    if logf:
        l = logging.handlers.RotatingFileHandler(logf, maxBytes=0,
                                                 backupCount=8, encoding='utf-8')
        fl = logging.Formatter(u'%(asctime)s %(levelname)s: %(message)s')
        l.setFormatter(fl)
        l.setLevel(logging.DEBUG)
        if rollover:
            l.doRollover()
        logger.addHandler(l)
    #
    cons = logging.StreamHandler()   # sys.stderr
    if logf:
        fc = logging.Formatter('[%(levelname)s]: %(message)s')
    else:
        fc = logging.Formatter('%(message)s')
    cons.setFormatter(fc)
    cons.setLevel(logging.CRITICAL)
    logger.addHandler(cons)
    # osxmkhomedir convention:
    logger.console_handler = cons
    return logger


def log_communicate(p, log):
    eof = [False, False]
    readfh = [p.stdout.fileno(), p.stderr.fileno()]
    while True:
        selfh = select.select(readfh, [], [])
        for fd in selfh[0]:
            if not eof[0] and fd == readfh[0]:
                inout = p.stdout.readline()
                if len(inout) == 0:
                    eof[0] = True
                    continue
                inout = inout.decode('utf8', 'replace').rstrip()           
                if len(inout) != 0:
                    log.debug(inout)
            if not eof[1] and fd == readfh[1]:
                inerr = p.stderr.readline()
                if len(inerr) == 0:
                    eof[1] = True
                    continue
                inerr = inerr.decode('utf8', 'replace').rstrip()
                if len(inerr) != 0:
                    log.error(inerr)
        if p.poll() != None:
            if eof[0] == True and eof[1] == True:
                break
    p.communicate()
    if p.returncode:
        log.error('Command returned error code %d', p.returncode)
    return p.returncode



def check_secure(login_script, log):
    isok = True
    if not os.path.exists(login_script):
        log.error('Does not exist: {0}'.format(login_script))
        return None
    if not os.path.isfile(login_script):
        log.error('Is not a regular file: {0}'.format(login_script))
        return False
    if not os.access(login_script, os.X_OK):
        log.error('Not executable: {0}'.format(login_script))
        isok = False
    login_script_stat = os.stat(login_script)
    if login_script_stat.st_uid:
        name = pwd.getpwuid(login_script_stat.st_uid).pw_name
        if (name not in grp.getgrnam('admin').gr_mem):
            log.error('Insecure script owner: {0}'.format(login_script))
            isok = False
    if grp.getgrgid(login_script_stat.st_gid).gr_name not in ('root', 'admin', 'wheel'):
        log.error('Insecure script group: {0}'.format(login_script))
        isok = False
    if (login_script_stat.st_mode & 0777) & ~0775:
        log.error('Insecure script permissions: {0}'.format(login_script))
        isok = False
    #
    login_dir = os.path.dirname(login_script)
    login_dir_stat = os.stat(login_dir)
    if login_dir_stat.st_uid:
        name = pwd.getpwuid(login_dir_stat.st_uid).pw_name
        if (name not in grp.getgrnam('admin').gr_mem):
            log.error('Insecure dir owner: {0}'.format(login_dir))
            isok = False
    if grp.getgrgid(login_dir_stat.st_gid).gr_name not in ('root', 'admin', 'wheel'):
        log.error('Insecure dir group: {0}'.format(login_dir))
        isok = False
    if (login_dir_stat.st_mode & 0777) & ~0775:
        log.error('Insecure dir permissions: {0}'.format(login_dir))
        isok = False
    return isok


