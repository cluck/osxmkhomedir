# -*- coding: utf-8 -*-

from __future__ import unicode_literals

__version__ = '4.0.0'
__author__ = 'Claudio Luck'
__author_email__ = 'claudio.luck@gmail.com'
__url__ = 'https://github.com/cluck/osxmkhomedir'

import sys
import os
import codecs
import fcntl
import getopt
import glob
import grp
import plistlib
import pwd
import select
import shutil
import subprocess

from osxmkhomedir.syncflow import State, Workflow
from osxmkhomedir import utils
from osxmkhomedir.utils import init_logging, log_communicate



def install(log):
    cmd = os.path.abspath(sys.argv[0])
    base = 'osxmkhomedir'
    template = u"""\
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>Disabled</key>
    <false/>
    <key>Label</key>
    <string>{base:s}</string>
    <key>ProgramArguments</key>
    <array>
      <string>osxmkhomedir-agent</string>
    </array>
    <key>RunAtLoad</key>
    <true/>
    <key>KeepAlive</key>
    <false/>
</dict>
</plist>
"""
    plist_file = '/Library/LaunchAgents/{base:s}.plist'.format(base='osxmkhomedir-agent')
    with codecs.open(plist_file, 'w', 'utf-8') as plist:
        plist.write(template.format(cmd=cmd, base='osxmkhomedir-agent'))
    os.chmod(plist_file, 0644)
    os.chown(plist_file, 0, 0)
    if not os.path.isdir('/Users/Shared/osxmkhomedir'):
        os.mkdir('/Users/Shared/osxmkhomedir', 0755)
    with open('/Users/Shared/osxmkhomedir/osxmkhomedir-agent.out', 'a') as f:
        os.chmod('/Users/Shared/osxmkhomedir/osxmkhomedir-agent.out', 0666)
    with open('/Users/Shared/osxmkhomedir/osxmkhomedir-agent.err', 'a') as f:
        os.chmod('/Users/Shared/osxmkhomedir/osxmkhomedir-agent.err', 0666)
    print('Written {0}'.format(plist_file))
    child = subprocess.Popen(['launchctl', 'load', plist_file],
        stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    log_communicate(child, log)
    #try:
    #   os.unlink(plist_file)
    #   log.info('Removed obsolete {0}'.format(plist_file))
    #except OSError:
    #    pass
    # Check/edit /etc/sudoers
    lines = (
        '\nALL ALL=(ALL) NOPASSWD: {cmd:s} --run*\n'.format(cmd=cmd),
        # lines to be replaced with the above:
        '\nALL ALL=(ALL) NOPASSWD: {cmd:s} --run\n'.format(cmd=cmd),
        '\nALL ALL=(root) NOPASSWD: {cmd:s} --run\n'.format(cmd=cmd),
    )
    fopts = os.O_EXCL | getattr(os, 'O_EXLOCK', 0)
    with open('/etc/sudoers', 'r', os.O_RDWR | fopts) as origf:
        try:
            fcntl.flock(origf, fcntl.LOCK_EX | fcntl.LOCK_NB)
        except IOError:
            log.warn('osxmkhomedir: /etc/sudoers busy, try again later')
            return 1
        sudoers0 = sudoers = origf.read()
        for line in lines[1:]:
           if lines[0] not in sudoers:
               sudoers = sudoers.replace(line, lines[0])
           else:
               sudoers = sudoers.replace(line, '\n# ' + line.lstrip('\n'))
        if lines[0] not in sudoers:
            sudoers += lines[0] + '\n'
        if sudoers == sudoers0:
            log.info('Already installed in /etc/sudoers')
            return
        with open('/etc/sudoers.tmp', 'w', fopts) as tmpf:        
            tmpf.write(sudoers)
        child = subprocess.Popen(['visudo', '-c', '-f', '/etc/sudoers.tmp'],
            stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        log_communicate(child, log)
        if not child.returncode:
            shutil.move('/etc/sudoers.tmp', '/etc/sudoers')
            os.chmod('/etc/sudoers', 0440)
            os.chown('/etc/sudoers', 0, 0)
            log.info('Written /etc/sudoers')
        else:
            log.error('Error building /etc/sudoers, not installed')
            os.unlink('/etc/sudoers.tmp')
            return 1
    child = subprocess.Popen(['defaults', 'write', 'com.apple.loginwindow',
        'LoginHook', '/usr/local/bin/osxmkhomedir-hook'],
        stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    log_communicate(child, log)


def usage(ret=0):
    print('{0}: {1}'.format(sys.argv[0], '[--install]'))
    return ret


class MakeHomedirWorkflow(Workflow):

    #def default(self):
    #    return dict(tasks = [('init',), ('sudo',), ('end',)])

    def select_task(self, conf, task):
        tt = task[0]
        if not self.args.agent:
            if os.getuid() == 0:
                return self.handle_task_or_wait(conf, ('sudo', 'roothook', 'loginpriv'), tt)
            else:
                return self.handle_task_or_wait(conf, ('userhook', 'login'), tt)
        else:
            return self.handle_task_or_wait(conf, ('agent',), tt)
        
    def task_init(self, conf):
        self.log.debug('TASK init')
        rn = 0
        hooks = dict()
        agents = dict()
        tasks = [('sudo',)]
        hrevs = glob.iglob('/usr/local/Library/osxmkhomedir/upgrade[0-9]*.sh')
        arevs = glob.iglob('/usr/local/Library/osxmkhomedir/agent[0-9]*.sh')
        for r in hrevs:
            rn = int(os.path.splitext(os.path.basename(r))[0].split('-', 1)[0][7:])
            hooks.setdefault(rn, [None, None])
            hooks[rn][int(r.endswith('-privileged.sh'))] = os.path.basename(r)
        for r in arevs:
            rn = int(os.path.splitext(os.path.basename(r))[0].split('-', 1)[0][5:])
            agents.setdefault(rn, None)
            agents[rn] = os.path.basename(r)
        for rn in hooks:
            if hooks[rn][1]:
                tasks.append(('roothook', hooks[rn][1]))
            if hooks[rn][0]:
                tasks.append(('userhook', hooks[rn][0]))
        if not self.args.no_login:
            if os.path.exists('/usr/local/Library/osxmkhomedir/login-privileged.sh'):
                tasks.append(('loginpriv',))
            if os.path.exists('/usr/local/Library/osxmkhomedir/login.sh'):
                tasks.append(('login',))                        
        for rn in agents:
            if agents[rn]:
                tasks.append(('agent', agents[rn]))
        return tasks

    def _task_hook(self, conf, script, t, l, x):
        self.log.debug('TASK %s: %s', t, script)
        if conf['status'] != 0:
            return self._task_error_skip(0, conf)
        rn = int(os.path.splitext(script)[0].split('-', 1)[0][l:])
        self.log.debug("%s %s", rn, conf['data'][x])
        if rn <= conf['data'][x]:
            self.log.debug('was already called: %s', script)
            return 0
        args = [os.path.join('/usr/local/Library/osxmkhomedir', script)]
        child = subprocess.Popen(args, env=os.environ,
            stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        utils.log_communicate(child, self.log)
        if child.returncode == 0:
            conf['data'][x] = rn
        return child.returncode

    def _task_script(self, conf, script, t):
        self.log.debug('TASK %s: %s', t, script)
        if conf['status'] != 0:
            return self._task_error_skip(0, conf)
        args = [os.path.join('/usr/local/Library/osxmkhomedir', script)]
        child = subprocess.Popen(args, env=os.environ,
            stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        utils.log_communicate(child, self.log)
        return child.returncode

    def task_loginpriv(self, conf):
        return self._task_script(conf, 'login-privileged.sh', 'loginpriv')
        
    def task_login(self, conf):
        return self._task_script(conf, 'login.sh', 'login')

    def task_roothook(self, conf, script):
        return self._task_hook(conf, script, 'roothook', 7, 'revision')
        
    def task_userhook(self, conf, script):
        return self._task_hook(conf, script, 'userhook', 7, 'revision')

    def task_agent(self, conf, script):
        return self._task_hook(conf, script, 'agent', 5, 'agent')
    





def main(argv):
    import argparse

    parser = argparse.ArgumentParser(description='osxmkhomedir')
    #
    parser.add_argument('-v', '--verbose', help='increment verbosity', action='count')
    parser.add_argument('-i', '--install', help='install %(prog)s', action='store_true')
    #parser.add_argument('--update', help='update %(prog)s', action='store_true')
    #
    parser.add_argument('-u', '--uid', type=int)
    parser.add_argument('-d', '--home', help='home directory')
    parser.add_argument('-s', '--state', help='state file')
    parser.add_argument('-l', '--log', help='log file')

    #parser.add_argument('--login', help='login (internal command)', action='store_true')
    parser.add_argument('--reset', help='reset config', action='store_true')
    parser.add_argument('--no-root', help='skip privileged scripts (internal command)', action='store_true')
    parser.add_argument('--no-login', help='skip login scripts', action='store_true')
    parser.add_argument('--sudo', help='also start privileged', action='store_true')
    parser.add_argument('--agent', help='run agent scripts on this instance', action='store_true')
    parser.add_argument('--root', help='run as root-hook', action='store_true')
    parser.add_argument('--user', help='run as user-hook', action='store_true')
    #
    args = parser.parse_args()

    #if args.update:
    #    import pip
    #    return pip.main(['install', '--upgrade', 'https://github.com/cluck/osxmkhomedir/archive/master.tar.gz'])

    if args.uid is None:
        args.uid = os.getuid()
        
    if args.home is None:
        args.home = pwd.getpwuid(args.uid).pw_dir
   
    if args.state is None:
        args.state = os.path.join(args.home, 'Library/Preferences/ch.cluck.osxmkhomedir.plist')
      
    if args.log is None:
        args.log = os.path.join(args.home, 'Library/Logs/osxmkhomedir.log')
 
    if args.log.startswith('-'):
        log = init_logging(None)
        log.console_handler.setLevel('DEBUG')
    else:
        log = init_logging(args.log)
        if not args.verbose:
            log.console_handler.setLevel('ERROR')
        elif args.verbose == 1:
            log.console_handler.setLevel('WARNING')
        elif args.verbose == 2:
            log.console_handler.setLevel('INFO')
        elif args.verbose >= 3:
            log.console_handler.setLevel('DEBUG')
        
    if args.install:
        return install(log)

    wf = MakeHomedirWorkflow(args, log, args.state, args.uid)
    ret = None
    while ret != 'end':    
        ret = wf.step()
    ret = wf.step()
    return
    
        
    if args.run:
        ret = run(args.uid, args, log)
        if ret:
            log.debug('Exiting with error %s', ret) 
        return ret

    print('osxmkhomedir {0}, Copyright (C) 2015, {1}'.format(__version__, __author__))
    parser.print_usage()
    return 1


def login_hook():
    """
    This is for com.apple.loginwindow LoginHook, to be installed by:
    
        defaults write com.apple.loginwindow LoginHook /path/to/script
    
    OS X will call this script as root, and the user logging in is passed in sys.argv[1].
    Default locale will be set to "C", PATH to a minimal subset and the cwd to /.
    """
    os.environ['PATH'] = '/usr/local/sbin:/usr/local/bin:' + os.environ.get('PATH', '')
    os.environ['LC_CTYPE'] = 'UTF-8'
    try:
        user = sys.argv[1]
        uid = pwd.getpwnam(user).pw_uid
    except:
        print('Usage: osxmkhomedir-hook <username>')
        sys.exit(1)
    login_script = '/usr/local/bin/osxmkhomedir'    
    log_file = os.path.expanduser('~/Library/Logs/osxmkhomedir.log')
    log = init_logging(log_file)
    #if check_secure(login_script, log):
    child = subprocess.Popen(['/usr/bin/sudo', '-n', '-u', user, login_script,
        '--run', '--no-root'], env={},
        stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    log_communicate(child, log)


def set_desktop():
    picture_path = sys.argv[1]
    #
    #import sqlite3
    #db = sqlite3.connect(os.path.expanduser('~/Library/Application Support/Dock/desktoppicture.db'))
    #db.execute('DELETE FROM data')
    #db.execute('INSERT INTO data (value) VALUES (?)', (picture_path,))
    #
    from AppKit import NSWorkspace, NSScreen
    from Foundation import NSURL
    # generate a fileURL for the desktop picture
    file_url = NSURL.fileURLWithPath_(picture_path)
    # make image options dictionary
    # we just make an empty one because the defaults are fine
    options = {}
    ws = NSWorkspace.sharedWorkspace()
    for screen in NSScreen.screens():
        (result, error) = ws.setDesktopImageURL_forScreen_options_error_(
                    file_url, screen, options, None
                )


def login_agent():
    os.environ['PATH'] = '/usr/local/sbin:/usr/local/bin:' + os.environ.get('PATH', '')
    os.environ['LC_CTYPE'] = 'UTF-8'
    child = subprocess.Popen(['/usr/local/Library/osxmkhomedir/agent.sh'], env={},
        stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    return log_communicate(child, log)
    

def command():
   sys.exit(main(sys.argv[1:]))

if __name__ == "__main__":
   sys.exit(main(sys.argv[1:]))

