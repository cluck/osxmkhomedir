# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import plistlib
import os
import sys
import pwd
import filelock
import uuid
import errno
import pprint


class TaskSpec(object):

    id_counter = 0

    def __init__(self, parent, name, data=None, defines=None):
        self.id = TaskSpec.id_counter = (TaskSpec.id_counter + 1 % 16384)  # atomic
        self._name = name
        self.inputs = []
        self.outputs = []
        self.data = data if data is not None else {}
        self.defines = defines if defines is not None else {}
        if parent is not None:
            parent._add_inner_task_spec(self)
        
    @property
    def name(self):
        return self._name
    
    @name.setter
    def name(self, name):
        old_name = self._name
        self._name = name
        self.rename_inner_task_spec(self, old_name)

    def connect(self, right_spec):
        """Connect this (left) to right."""
        self.outputs.append(right_spec)
        right_spec._connect_spec_from(self)

    def _connect_spec_from(self, left_spec):
        self.inputs.append(left_spec)

    def __repr__(self):
        return '<TaskSpec object (%s) at %s>' % (self._name, hex(id(self)))


class StartTaskSpec(TaskSpec):
    pass


class ExitTaskSpec(TaskSpec):
    pass


class WorkflowSpec(TaskSpec):

    def __init__(self, parent, name, data=None, defines=None):
        self.task_specs = {} # API
        TaskSpec.__init__(self, parent, name, data=None, defines=None)
        self.start_task_spec = self.create_workflow()
                
    def create_workflow(self):
        root_spec = TaskSpec(self, 'Root')
        #start_spec = StartTaskSpec(self, 'Init')
        #exit_spec = ExitTaskSpec(self, 'Exit')
        #root_spec.connect(start_spec)
        #start_spec.connect(exit_spec)
        return root_spec

    def _add_inner_task_spec(self, tspec):
        self.task_specs[tspec.name] = tspec

    def rename_inner_task_spec(self, tspec, old_name):
        self.task_specs[tspec.name] = tspec
        del self.task_specs[old_name]

    def __repr__(self):
        return '<WorkflowSpec object (%s) at %s>' % (self._name, hex(id(self)))


# ---------

class Task(object):

    MAYBE     =  1
    LIKELY    =  2
    FUTURE    =  4
    WAITING   =  8
    READY     = 16
    COMPLETED = 32
    CANCELLED = 64

    FINISHED_MASK      = CANCELLED | COMPLETED
    DEFINITE_MASK      = FUTURE | WAITING | READY | FINISHED_MASK
    PREDICTED_MASK     = FUTURE | LIKELY | MAYBE
    NOT_FINISHED_MASK  = PREDICTED_MASK | WAITING | READY
    ANY_MASK           = FINISHED_MASK | NOT_FINISHED_MASK

    state_names = {
        FUTURE:    'FUTURE',
        WAITING:   'WAITING',
        READY:     'READY',
        CANCELLED: 'CANCELLED',
        COMPLETED: 'COMPLETED',
        LIKELY:    'LIKELY',
        MAYBE:     'MAYBE'
    }
    
    id_counter = 0

    @classmethod
    def Iterator(cls, current, filter=None):
        for start_task in current.children:
            yield start_task
            for task in start_task.children:
                ignore_task = False
                if filter is not None:
                    search_predicted = filter & cls.LIKELY != 0
                    is_predicted = current.state & cls.LIKELY != 0
                    ignore_task = is_predicted and not search_predicted
                if not ignore_task:
                    yield task

    def __init__(self, task_spec, parent, blocking=None, state=MAYBE):
        assert isinstance(task_spec, TaskSpec)
        assert isinstance(parent, (type(None), Task))
        #
        self.id = Task.id_counter = (Task.id_counter + 1 % 65535)  # atomic
        self.parent = parent
        self.spec = task_spec
        self.blocking = blocking
        self.state = state
        self.blocked_tasks = []
        self.data = {}
        self.internal_data = {}
        if blocking is not None:
            blocking._add_blocking_task(self)

    @property
    def name(self):
        return '{0}{1}'.format(self.spec._name, self.id)

    #def _has_state(self, state):
    #    return (self.state & state) != 0

    def get_state_repr(self):
        state_name = []
        for state, name in self.state_names.iteritems():
            if ((self.state & state) != 0):
                state_name.append(name)
        return '|'.join(state_name)

    def _add_blocking_task(self, task):
        assert isinstance(task, Task)
        self.blocked_tasks.append(task)

    def _add_blocked_task(self, task_spec, state=MAYBE):
        task = Task(task_spec, self.parent, self, state=state)
        if state == self.READY:
            task._ready()

    def _ready(self):
        print('READY')

    def cancel(self):
        del self.data[:]
        self.state = Task.CANCELLED

    def complete(self):
        raise NotImplementedError

    def complete_(self):
        assert self.state & Task.NOT_FINISHED_MASK == self.state
        try:
            self.complete()
        except BaseException as e:
            self.state = Task.CANCELLED
            raise
        else:
            self.state = Task.COMPLETE
        finally:
            self.data = {}

    def __repr__(self):
        return '<Task object (%s) in state %s at %s>' % (
            self.spec.name,
            self.get_state_repr(),
            hex(id(self)))



class Workflow(Task):
    
    def __init__(self, wf_spec, parent=None, blocking=None, state=Task.MAYBE):
        Task.__init__(self, wf_spec, parent, blocking, state)
        #
        r = wf_spec.start_task_spec
        assert isinstance(r, TaskSpec)
        self.task_tree = Task(r, self)
        #
        self.task_tree.state = Task.COMPLETED
        start = self.task_tree._add_blocked_task(
                                self.spec.start_task_spec, state=Task.FUTURE)

    def __repr__(self):
        return '<%s %s in state %s>' % (
            type(self).__name__, self.spec, self.get_state_repr(), 
        )

# =========

if __name__ == "__main__":
    wfspec = WorkflowSpec(None, 'w')
    print(wfspec)
    for tspec in wfspec.task_specs.items():
        print(tspec)
    #
    print
    wf = Workflow(wfspec)
    print(wf)
    try:
        wf.complete_task_from_id('Start')
    except:
        pass
    print(wf)    




class Wor2kflow(object):

    def __init__(self, args, log, conf_file, uid, **kw):
        self.wait_time = 60
        self.log = log
        self.args = args
        self.conf_file = conf_file
        self.uid = uid
        self.conf_lock = filelock.FileLock(self.conf_file + '.lockfile')
        if os.getuid() == 0:
            os.chown(self.conf_file + '.lockfile', uid, 0)
        self.kw = kw
        self.session = False
        self.conf = self._default(**self.kw)
        #
    
    def _default(self, **kw):
        ret = dict(
            version = 2,
            session = 0,
            data = dict(
                agent = 0,
                revision = 0,
            ),
            status = 0,
            tasks = [('init',), ('end',)],
            task = 0,
        )
        ret.update(kw)
        return ret
 
    def writePlist(self, obj, f):
        plistlib.writePlist(obj, f)
        if os.getuid() == 0:
            os.chown(f, self.uid, 0)
 
    def step(self):
        #if len(self.conf['tasks']) <= self.conf['task']:
        #    return 'end'
        task_run = None
        try:
            with self.conf_lock.acquire(timeout=self.wait_time):
                conf = self._default(**self.kw)
                conf['data'] = self.conf['data']
                conf['status'] = 0
                stored_conf = dict(session=0, version=2)
                try:                    
                   stored_conf = plistlib.readPlist(self.conf_file)
                except IOError:
                    pass
                version = int(stored_conf.get('version', 1))
                for v in range(version, conf['version']):
                    stored_conf = getattr(self, 'convert{0:d}'.format(v))(stored_conf, conf)
                if self.session is False:
                    self.session = stored_conf.get('session', 0)
                    if self.session == 0:
                        self.session = stored_conf['session'] = uuid.uuid4().get_hex()
                elif self.session != stored_conf.get('session', 0):
                    raise RuntimeError('session mismatch')
                conf.update(stored_conf)
                try:
                    if len(conf['tasks']) < conf['task']:
                        return 'end'
                    self._select_task(conf)
                    ret = self._run_task(conf)
                    conf['status'] = ret = ret or 0
                    if ret:
                        self.log.error('{0} (-> {1}) returned error {2}'.format(task[0], task_run, ret))
                except SystemExit as e:
                    conf['status'] = e.code or errno.EINTR
                    plistlib.writePlist(conf, self.conf_file) 
                    self.conf = conf
                    return 'end'
                except BaseException:
                    conf['status'] = errno.EINTR
                    plistlib.writePlist(conf, self.conf_file)
                    self.conf = conf
                    raise
                #
                if task_run:
                    plistlib.writePlist(conf, self.conf_file)
                    self.conf = conf    
        except filelock.Timeout:
            return 'timeout'
        return task_run

    def convert1(self, conf, dfl):
        conf = dict(version=2, session=dfl['session'],
            task=dfl['task'], tasks=dfl['tasks'],
            data=dict(agent=0, revision=conf['revision']))
        return conf
        
    def _select_task(self, conf):
        task = conf['tasks'][conf['task']]
        if task[0] == 'error':
            return 'end'
        if task[0] in ('init', 'end'):
            return task[0]
        ret = self.select_task(conf, task)
        if not ret:
            raise RuntimeError('invalid task')
        return ret
          
    def have_pending_tasks(self, conf, tasks, t=None):
        """Check if there are still tasks of some type: `list(tasks)`."""
        t = t if t else conf['task']
        for i in range(t, len(conf['tasks'])):
            if conf['tasks'][i][0] in tasks:
                return True
        return False
        
    def handle_task_or_wait(self, conf, tasks, task):
        if not self.have_pending_tasks(conf, tasks):
            self.log.debug('no pending tasks, exiting')
            return 'end'
        if task in tasks:
            return task
        return 'wait'

    def _task_init(self, conf):
        tasks = [('init',)]
        tasks.extend(self.task_init(conf))
        tasks.append(('end',))
        conf['tasks'][:] = tasks
        return 0

    def task_wait(self, conf):
        pass

    def task_end(self, conf):
        pass

    def _run_task(self, conf, task_run):
        if task_run == 'init':
            F = self._task_init
        else:
            F = getattr(self, 'task_' + task_run)
        task = conf['tasks'][conf['task']]
        try:
            if len(conf['tasks']) <= conf['task']:
                conf['task'] = len(conf['tasks']) - 1
                return 0
                
                
                
            self._select_task(conf)
            ret = self._run_task(conf)
            conf['status'] = ret = ret or 0
            if ret:
                self.log.error('{0} (-> {1}) returned error {2}'.format(task[0], task_run, ret))
        except SystemExit as e:
            conf['status'] = e.code or errno.EINTR
            plistlib.writePlist(conf, self.conf_file) 
            self.conf = conf
            return 'end'
        except BaseException:
            conf['status'] = errno.EINTR
            plistlib.writePlist(conf, self.conf_file)
            self.conf = conf
            raise
                
        
        
        
        return F(conf, *task[1:])

    def _task_error_skip(self, ret, conf):
       log.debug('! skipping due to previous error:')
       return ret
