# -*- coding: utf-8 -*-

# app.py

import sys
import os
import filelock
import plistlib
import glob
import uuid
import time
import errno


class State(object):

    def __init__(self, args, **kw):
        self.wait_time = 60
        self.args = args
        self.conf_file = os.path.expanduser('~/Library/Preferences/ch.cluck.osxmkhomedir.plist')
        self.conf_lock = filelock.FileLock(self.conf_file + '.lockfile')
        self.kw = kw
        self.session = False
        self.conf = self.default(**self.kw)
        self.step()
    
    def default(self, **kw):
        ret = dict(
            version = 2,
            session = 0,
            data = dict(
                agent = 0,
                revision = 0,
            ),
            status = 0,
            tasks = [('init', 'end')],
            task = 0,
        )
        ret.update(kw)
        return ret
 
    def step(self, log):
        if len(self.conf['tasks']) <= self.conf['task']:
            return 'end'
		task_run = None
        try:
			with self.conf_lock.acquire(timeout=self.wait_time):
				conf = self.default(**self.kw)
				conf['data'] = self.conf['data']
				conf['status'] = 0
				stored_conf = dict(session=0, version=2)
				try:
					stored_conf = plistlib.readPlist(self.conf_file)
				except IOError:
					pass
				version = int(stored_conf.get('version', 1))
				for v in range(version, conf['version']):
					stored_conf = getattr(self, 'convert{0:d}'.format(v))(stored_conf, conf, log)
				if self.session is False:
					self.session = stored_conf.get('session', 0)
					if self.session == 0:
						self.session = stored_conf['session'] = uuid.uuid4().get_hex()
				elif self.session != stored_conf.get('session', 0):
					raise RuntimeError('session mismatch')
				conf.update(stored_conf)
				try:
					if len(conf['tasks']) <= conf['task']:
						return 'end'
					data = conf['data']
					task = conf['tasks'][conf['task']]
					task_run = self._select_task(task, conf, log)
					ret = self.run_task(task, conf, task_run, log)
					if ret != 0:
						log.error('{0} ({1}) returned error {2}'.format(task[0], task_run, ret))
					conf['status'] = ret
				except SystemExit as e:
					conf['status'] = e.code or errno.EINTR
					plistlib.writePlist(conf, self.conf_file) 
					self.conf = conf
					return 'end'
				except BaseException:
					conf['status'] = errno.EINTR
					plistlib.writePlist(conf, self.conf_file)
					self.conf = conf
					raise
				#
				if task_run:
					conf['task'] += 1
					plistlib.writePlist(conf, self.conf_file)
					self.conf = conf    
        except filelock.Timeout:
    		return 'timeout'
    	if not task_run:
    		self.task_wait(log)
        return task_run

    def convert1(self, conf, dfl, log):
        conf = dict(version=2, session=dfl['session'],
            task=dfl['task'], tasks=dfl['tasks'],
            data=dict(agent=0, revision=conf['revision']))
        return conf
    
    def have_pending_tasks(self, conf, tasks, t=None):
        """Check if there are still tasks of some type: `list(tasks)`."""
        t = t if t else conf['task']
        for i in range(t, len(conf['tasks'])):
            if conf['tasks'][i][0] in tasks:
                return True
        return False
    
    def task_wait(self, conf, log):
    	pass

	def task_end(self, conf, log):
		pass

    def run_task(self, taskobj, conf, task=None):
        task = task if task else taskobj[0]
        F = getattr(self, 'task_' + task)
        return F(conf, *taskobj[1:])

    def _task_error_skip(self, ret, conf):
       log.debug('! skipping due to previous error:')
       return ret

    # ---
    
    def _select_task(self, task, conf, log):
    	if task[0] == 'error':
    		return 'end'
    	if task[0] in ('init', 'end'):
    		return task[0]
    	ret = self.select_task(task, conf, log)
    	if not ret:
    		raise RuntimeError('invalid task')
    	return ret

    def task_init(self, conf, log):
        log.debug('TASK init')
        rn = 0
        hooks = dict()
        agents = dict()
        inittask = conf['tasks'][0]
        tasks = [inittask]
        hrevs = glob.iglob('/usr/local/Library/osxmkhomedir/upgrade[0-9]*.sh')
        arevs = glob.iglob('/usr/local/Library/osxmkhomedir/agent[0-9]*.sh')
        for r in hrevs:
            rn = int(os.path.splitext(os.path.basename(r))[0].split('-', 1)[0][7:])
            hooks.setdefault(rn, [None, None])
            hooks[rn][int(r.endswith('-privileged.sh'))] = os.path.basename(r)
        for r in arevs:
            rn = int(os.path.splitext(os.path.basename(r))[0].split('-', 1)[0][5:])
            agents.setdefault(rn, None)
            agents[rn] = os.path.basename(r)
        for rn in hooks:
            if hooks[rn][1]:
                tasks.append(('roothook', hooks[rn][1]))
            if hooks[rn][0]:
                tasks.append(('userhook', hooks[rn][0]))
        for rn in agents:
            if agents[rn]:
                tasks.append(('agent', agents[rn]))
        conf['tasks'][:] = tasks
        return 0

    def task_roothook(self, conf, log, script):
        log.debug('TASK roothook %s', script)
        if conf['status'] != 0:
            return self._task_error_skip(0, conf)        
        return 0

    def task_userhook(self, conf, log, script):
        log.debug('TASK userhook %s', script)
        if conf['status'] != 0:
            return self._task_error_skip(0, conf)        
        rn = int(os.path.splitext(script)[0].split('-', 1)[0][7:])
        if rn <= conf['data']['revision']:
            log.info('skipped userhook %s', script)
            return 0
        log.debug('RUNNING userhook %s', script)
        ret = int(rn > 2)
        if ret == 0:
            conf['data']['revision'] = rn
        return ret
        
    def task_agent(self, conf, log, script):
        log.debug('TASK agent %s', script)
        #if not self.args.agent:
        if conf['status'] != 0:
            return self._task_error_skip(0, conf)
        rn = int(os.path.splitext(script)[0].split('-', 1)[0][5:])
        if rn <= conf['data']['agent']:
            log.info('skipped agent %s', script)
            return 0
        log.debug('RUNNING agent %s', script)
        ret = int(rn > 2)
        if ret == 0:
            conf['data']['agent'] = rn
        return ret


def main():

    import argparse

    parser = argparse.ArgumentParser(description='osxmkhomedir')
    #
    parser.add_argument('-v', '--verbose', help='increment verbosity', action='count')
    parser.add_argument('-i', '--install', help='install %(prog)s', action='store_true')
    #parser.add_argument('--update', help='update %(prog)s', action='store_true')
    #
    parser.add_argument('--agent', help='run as agent', action='store_true')
    parser.add_argument('--root-hook', help='run as root-hook', action='store_true')
    parser.add_argument('--user-hook', help='run as user-hook', action='store_true')
    #
    args = parser.parse_args()

    c = State(args=args)
    ret = None
    while ret not in ('end', 'error'):    
        ret = c.step()


sys.exit(main())
